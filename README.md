# About

Used to test calls to the Spotify API using Basic Authentication to obtain a Bearer token. Requires a Spotify subscription (free works), and an application in the Spotify for Developers dashboard to obtain the Client ID and Client Secret from.

## Usage

1. Create a file with two lines: first line with the Client ID, and second line with the Client Secret
2. Use by executing the following command:
    1. python.exe { Path to folder containing modules from this repo }/main.py { Path to file with client tokens }
    2. Example: python.exe "c:/Repo/main.py" "clienttokens.txt"
3. Follow the prompts
