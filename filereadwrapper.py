from bpmclasses import ClientTokens

class ClientTokenFileReadWrapper:
    """Wraps read functions related to reading client tokens"""

    def get_client_tokens(self,client_file_path):
        client_id = ""
        client_secret = ""
        with open(client_file_path, 'r') as client_file:
            client_id = client_file.readline().rstrip()
            client_secret = client_file.readline().rstrip()
        return_token = ClientTokens(client_id, client_secret)
        return return_token