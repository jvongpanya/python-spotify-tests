import json
import requests
from spotifyapibase import SpotifyApiBase

class SpotifySearchApi(SpotifyApiBase):
    """API wrapper for Spotify search functionality"""

    _divider = "||"

    def __init__(self, access_token):
        SpotifyApiBase.__init__(self, access_token)
        self.api_search_url = "https://api.spotify.com/v1/search"

    def search_for_song(self, song_search_string, limit=10, offset=0):
        """
        Searches for the song by the search string, and prints out the list of songs with their IDs. Returns ID selected by the user
        song_search_string will have spaces automatically escaped
        Refer to the Spotify search API to understsand how song_search_string should be formatted
        """

        query_dictionary = self._create_query_string_dictionary(song_search_string, limit, offset, "track")

        try:
            response = requests.get(self.api_search_url, headers=self.headers, params=query_dictionary)
            if response.status_code != 200:
                raise Exception("Bad response:", response.status_code)
            else:
                song_selection = self._build_song_map_from_response_text(response.text)
                song_selected = input("Select a song by the ID (first number per line): ")
                print()
                return song_selection[int(song_selected)]
        except:
            raise
    
    def _create_query_string_dictionary(self, song_search_string, limit, offset, field_type):
        query_dictionary = { "query": song_search_string, "limit": limit, "offset": offset, "type": field_type }
        return query_dictionary

    def _build_song_map_from_response_text(self, response_text):
        """Creates a song map, and returns it. Prints it out in a readable format."""
        response_object = json.loads(response_text)
        song_counter = 0
        song_selection = {}
        print("Selection ID", type(self)._divider, "Song Name", type(self)._divider, "Album Title", type(self)._divider, "Song Artist", type(self)._divider, "Spotify Song ID")
        for track in response_object["tracks"]["items"]:    
            album = track["album"]["name"]
            artist = track["artists"][0]["name"]
            song = track["name"]
            song_id = track["id"]
            song_selection[song_counter] = song_id
            print(str(song_counter), type(self)._divider, song, type(self)._divider, album, type(self)._divider, artist, type(self)._divider, song_id)
            song_counter += 1
        print()
        return song_selection
