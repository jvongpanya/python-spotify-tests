import json
import requests

class SpotifyApiBase:
    """Base class for Spotify API wrapper"""

    def __init__(self, access_token):
        if access_token is None or access_token.access_token == "":
            raise ValueError("access_token is either null, or does not have a valid access_token value")
        self.headers = { "Authorization": "Bearer " + access_token.access_token }