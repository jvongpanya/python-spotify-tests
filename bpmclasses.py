
class ClientTokens:
    """Container for client ID and secret"""

    def __init__(self, client_id, client_secret):
        self.client_id = client_id
        self.client_secret = client_secret

class AccessToken:
    """Represents the access token response from authentication APIs"""

    def __init__(self, access_token: str, token_type: str, expires_in: int, scope: str):
        self.access_token = access_token
        self.token_type = token_type
        self.expires_in = expires_in
        self.scope = scope