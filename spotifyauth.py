import requests
import json
import base64
from bpmclasses import AccessToken

class SpotifyAuthenticator:
    """Used to authenticate to Spotify"""

    def __init__(self):
        self.api_token_url = "https://accounts.spotify.com/api/token"
        self.api_data = "grant_type=client_credentials" 
        self.headers = { "Authorization": "Basic ", "Content-Type":  "application/x-www-form-urlencoded"}

    def get_bearer_token(self, client_id, client_secret):
        """Obtains the bearer token, on 200 response, or None"""
        try:
            # Creates the new auth token and appends it to the header
            base64_auth = self._get_base64(client_id=client_id, client_secret=client_secret)
            self.headers["Authorization"] = self.headers["Authorization"] + base64_auth

            response = requests.post(self.api_token_url, data=self.api_data, headers=self.headers)

            if response.status_code != 200:
                raise Exception("Base response:", response.status_code)
            else:
                new_token = AccessToken(**json.loads(response.text))
                return new_token
        except:
            raise
        
    def _get_base64(self, client_id, client_secret):
        """Generates the base64 version of the provided client values"""
        auth_to_encode = client_id + ":" + client_secret
        auth_bytes = auth_to_encode.encode()
        encoded_auth_bytes = base64.b64encode(auth_bytes)
        base64_string = encoded_auth_bytes.decode()
        return base64_string
