from spotifyauth import SpotifyAuthenticator
from bpmclasses import AccessToken
from filereadwrapper import ClientTokenFileReadWrapper
from spotifysearchapi import SpotifySearchApi
from spotifyaudiofeaturesapi import SpotifyAudioFeaturesApi
import json
import sys

_continue_search = "y"

if len(sys.argv) <= 1:
    raise ValueError("An argument is required for the file path")

client_file_path = sys.argv[1]

# Get tokens
print("Getting tokens...")
token_reader = ClientTokenFileReadWrapper()
token = token_reader.get_client_tokens(client_file_path)

# Authenticate
print("Authenticating...")
authenticator = SpotifyAuthenticator()
token = authenticator.get_bearer_token(token.client_id, token.client_secret)


search_api = SpotifySearchApi(token)
audio_feature_api = SpotifyAudioFeaturesApi(token)

# Search loop
print("Starting search. Press CTRL+C (or applicable command to interrupt this script) to stop at any time...")
keep_search = _continue_search
while keep_search == _continue_search:

    # Do search and get song ID
    search_string = input("Please provide a song name to search for: ")
    print("Searching...")
    print()
    song_id = search_api.search_for_song(search_string)

    # Get audio features
    print("Getting audio features...")
    audio_feature_api.get_audio_features(song_id)

    # Reset?
    keep_search = input("Continue? y = yes, anything else = no: ")
    print()