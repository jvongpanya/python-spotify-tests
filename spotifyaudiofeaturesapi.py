import json
import requests
from spotifyapibase import SpotifyApiBase

class SpotifyAudioFeaturesApi(SpotifyApiBase):
    """API wrapper for Spotify audio feature search functionality"""

    def __init__(self, access_token):
        SpotifyApiBase.__init__(self, access_token)
        self.api_features_url = "https://api.spotify.com/v1/audio-features/"

    def get_audio_features(self, song_id):
        """
        Obtains a song's features by song_id, and prints it out
        """

        if song_id == "":
            raise ValueError("song_id must not be blank")

        try:
            new_url = self.api_features_url + song_id
            response = requests.get(new_url, headers=self.headers)
            if response.status_code != 200:
                raise Exception("Bad response:", response.status_code)
            else:
                print(response.text)
                print()
        except:
            raise